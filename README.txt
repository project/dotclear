
Dotclear modules
================
http://drupal.org/project/dotclear

OVERVIEW
--------
This project contains two modules:
- dc_import.module - to convert your dotclear site into a drupal site
- dc_filter.module - to display correctly imported dotclear posts

WARNING
-------
The import module has only been tested with dotclear 1.2.3 and drupal 5.3.

DON'T BE SILLY, TEST THE IMPORT ON A TEST SERVER FIRST, NOT ON YOUR LIVE SITE. USE IT AT YOUR OWN RISK.

INSTALL / IMPORT
----------------
1. If not already done, install and configure a new drupal site.
2. Place the entire dotclear modules folder into your modules directory (eg: sites/all/modules/).
3. Create a subdirectory 'dc' in the files directory of drupal, and copy the dotclear 'images' directory
into it. So you should have this directory: files/dc/images/
4. Go to administer -> site building -> modules, and enable the dc_filter and dc_import modules.
5. Go to administer -> site configuration -> input format > add input format, and create a new
input format called 'Dotclear' that uses the 'Dotclear' filter.
6. Go to administer -> site configuration -> dotclear import settings, and configure the settings.

There is no UI for launching the import, so you have to do it manually:
- Import users by browsing to: admin/settings/dc_import/import/users.
- Import categories by browsing to: admin/settings/dc_import/import/categories.
- Import posts by browsing to: admin/settings/dc_import/import/posts.

You can then disable the dc_import.module, and uninstall it to destroy temporary data, but do NOT disable
the dc_filter.module.

PATHS
-----
You can modify the .htaccess file in drupal directory to redirect old dotclear paths to new drupal paths.

Examples:
* For posts with format: http://yourserver/?index.php/2007/11/02/344-foo-bar

  RewriteCond %{QUERY_STRING} ^([0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9]+-)(.*)$
  RewriteRule index.php/ /%2? [L,R=301]

* For posts with format: http://yourserver/index.php/2007/11/02/344-foo-bar

  RedirectMatch permanent /index.php/([0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9]+)-(.*)$ /$2

* For main feed:

  RewriteRule ^rss.php /rss.xml [R=301,L]

TODO
----
- Add the UI to launch the import.
- When fixing references to images, make the 'dc' subdirectory name configurable.
- Configure Dotclear input format when installing dc_filter.module.

Credits / Contacts
------------------
Aymerick Jehanne <aymerick@jehanne.org>
http://aymerick.com
